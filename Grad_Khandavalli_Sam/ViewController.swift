//
//  ViewController.swift
//  Grad_Khandavalli_Sam
//
//  Created by Sam Sreetej Khandavalli on 12/4/18.
//  Copyright © 2018 Sam Sreetej Khandavalli. All rights reserved.
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var imagedisplay: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //presents photo library to choose the image
    @IBAction func openLib(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.allowsEditing = false
        picker.delegate = self
        picker.sourceType = .photoLibrary
        present(picker, animated: true)
    }
}

extension ViewController: UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    //override for imagepicker controller
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info["UIImagePickerControllerOriginalImage"] as? UIImage else {
            return
        }
        imagedisplay.image = image
        descLabel.text = "Thinking..."
        
        // load the ml core model
        guard let model = try? VNCoreMLModel(for: boxcar().model) else {
            fatalError("Unable to load model")
        }
        // Create a vision request
        let request = VNCoreMLRequest(model: model) { [weak self] request, error in
            guard let results = request.results as? [VNClassificationObservation],
                let topResult = results.first
                else {
                    fatalError("Unexpected results")
            }
            // Result display
            DispatchQueue.main.async { [weak self] in
                self?.descLabel.text = "It is \(topResult.identifier) with \(Int(topResult.confidence * 100))% confidence"
            }
        }
        guard let ciImage = CIImage(image: image)
            else { fatalError("Cannot read picked image")}
        // image classification
        let handler = VNImageRequestHandler(ciImage: ciImage)
        DispatchQueue.global().async {
            do {
                try handler.perform([request])
            } catch {
                print(error)
            }
        }
    }
}
